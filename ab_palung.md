# Apakah palung itu?

![Peta kedalaman Palung Mariana](https://upload.wikimedia.org/wikipedia/commons/thumb/8/85/Pseudoliparis_swirei_in_Mariana_Trench_map.jpg/1600px-Pseudoliparis_swirei_in_Mariana_Trench_map.jpg)

Gambar 1 Peta kedalaman Palung Mariana ([Commons Wikimedia, CC-0](https://upload.wikimedia.org/wikipedia/commons/thumb/8/85/Pseudoliparis_swirei_in_Mariana_Trench_map.jpg/1600px-Pseudoliparis_swirei_in_Mariana_Trench_map.jpg)). Warna merah menunjukkan daerah yang dangkal (tidak dalam) dan warna biru adalah daerah yang dalam.

## Pembuka

Apakah adik-adik pernah menonton film [Journey to the center of the earth](https://en.wikipedia.org/wiki/Journey_to_the_Center_of_the_Earth) yang dibuat berdasarkan buku karangan [Jules Verne](https://en.wikipedia.org/wiki/Jules_Verne) yang diterbitkan pertama kali tahun 1864, atau film berjudul [The core](https://en.wikipedia.org/wiki/The_Core). Kedua film itu temanya sama, perjalanan menuju pusat (inti) bumi. Kalau belum, silahkan tonton ya, tentunya bersama kakak atau orang tua.

## Palung: titik terdalam di bumi

Palung adalah bentang alam di bawah laut, berupa jurang yang sangat dalam. Saking dalamnya, dalam film The core, dikisahkan bahwa untuk mencapai inti bumi dengan cepat adalah melalui palung. Sebenarnya seberapa dalam sebuah palung itu? Berikut tabel datanya ([Wikipedia/oceanic_trench](https://en.wikipedia.org/wiki/Oceanic_trench)). Adik-adik juga bisa melihat Gambar 1 di atas yang menunjukkan perbedaan kedalaman dasar laut di sepanjang area Palung Mariana.



Tabel 1 Data kedalaman palung ([Wikipedia/oceanic_trench](https://en.wikipedia.org/wiki/Oceanic_trench))

| Palung   | Lokasi (samudra) | Kedalaman (m) |
| -------- | ---------------- | ------------- |
| Mariana  | Pasifik          | 11.034        |
| Tonga    | Pasifik          | 10.882        |
| Filipina | Pasifik          | 10.545        |
| Jepang   | Pasifik          | 10.375        |



Sangat dalam bukan. Bandingkan dengan Gunung Everest yang tingginya hanya 8848 m. Untuk Palung Mariana yang dipercaya terdalam sedunia, 11 ribu km, itu sama dengan 83 Tugu Monas disusun ke atas. Panjangnya sendiri membentang 2542 meter ([Livescience](https://www.livescience.com/23387-mariana-trench.html)). Karena sangat dalam, maka untuk mencapainya, kita memerlukan kapal selam khusus. Adalah James Cameron dan beberapa orang kru, pada tahun 2012, pernah mencapai kedalaman 11.000 meter di Palung Mariana menggunakan kapal selam canggih bernama Deepsea Challenger ([Natgeo](https://news.nationalgeographic.com/news/2012/04/120405-james-cameron-mariana-trench-deepsea-challenger-oceans-science/)). Untuk menuruninya, diperlukan waktu tidak kurang dari satu jam. 

## Lantas palung itu sebenarnya apa?

Palung adalah batas penunjaman (atau penyusupan) lempeng. Jadi bentuknya memanjang, semacam parit di bawah laut yang memanjang mengikuti batas penunjaman (lihat gambar di bawah ini).  Lihat bagian yang melingkar paling luar dengan tulisan "trench"/palung (lihat Gambar 2 berikut ini). Adik-adik bisa melihat jalur "parit" yang melingkar di bagian luar.



![Gambar x Gambar 3D zona penunjaman Palung Mariana](https://oceanexplorer.noaa.gov/okeanos/explorations/ex1605/background/geology/media/fig3-800.jpg)

Gambar 2 Gambar 3D kawasan zona penunjaman yang menunjukkan posisi Palung Mariana di cincin paling luar, lihat tulis "trench" (Hak Cipta foto ini ada pada [Susan Merle](https://www.pmel.noaa.gov/people/susan-merle) dari NOAA Pacific Marine Environmental Laboratory)



Ingat ya kalau kulit bumi terdiri dari lempengan padat yang saling bergerak satu sama lain. Ada lempeng yang saling bertabrakan, ada yang saling menjauh, ada pula yang saling menyerempet.  Nah palung terjadi pada sistem lempeng yang saling bertabrakan. Ingat pula bahwa pada zona tabrakan itu, terjadi penunjaman, atau masuknya lempeng yang berat jenisnya lebih berat (biasanya lempeng samudra) di bawah lempeng yang lebih ringan (biasanya lempeng benua). Adik-adik dapat membaca jawaban pada [tautan ini](http://anakbertanya.com/mengapa-di-indonesia-banyak-gunung-berapi/). 

## Apa saja yang ada di palung?

Di palung, juga berkembang ekosistem. Jadi selain bebatuan juga ada banyak makhluk hidup, ada ikan, udang-udangan, dan organisme bersel satu ([IFLScience](https://www.iflscience.com/plants-and-animals/what-lives-marianas-trench/)) (Gambar 3). Makhluk hidup-makhluk hidup tersebut telah mengembangkan sistem tubuh agar dapat hidup di lingkungan ekstrim di Palung Mariana. Lingkungan yang sangat gelap dengan tekanan yang bisa ratusan bahkan ribuan kali lebih besar dibanding tekanan di permukaan.



![makhluk hidup di dasar palung](https://oceanexplorer.noaa.gov/okeanos/explorations/ex1605/background/geology/media/fig5-800.jpg)

Gambar 3 Cangkang kerang yang berserak di dasar Palung Mariana ([IFLScience](https://www.iflscience.com/plants-and-animals/what-lives-marianas-trench/))