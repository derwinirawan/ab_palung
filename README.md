# ab_palung

Repositori untuk menyimpan naskah dan berkas pendukung jawaban untuk pertanyaan tentang "palung". Naskah ini akan dikirimkan ke anakbertanya.com. Pertanyaan ini berasal dari pertanyaan anak-anak usia SD dari seluruh Indonesia. Silahkan langsung membuka [tautan ini](https://gitlab.com/derwinirawan/ab_palung/blob/master/ab_palung.md). 